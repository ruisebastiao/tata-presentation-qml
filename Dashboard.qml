import QtQuick 2.2
import QtQuick.Extras 1.4

Item {

    id: dashboard

    width: parent.width
    height: parent.height / 5
    anchors.bottom: parent.bottom

    property int slideCount
    property int currentSlide

    property int animTime: 2000

    function turnLeft() {
        turnLeftAnim.restart();
        rpmAnim.restart();
    }

    function turnRight() {
        turnRightAnim.restart();
        rpmAnim.restart();
    }

    Item {
        id: container
        width: dashboard.width
        height: Math.min(dashboard.width, dashboard.height)
        anchors.centerIn: parent

        Rectangle {
            width: gaugeRow.width * 1.2
            height: container.height * 2
            color: "#161616"
            anchors.horizontalCenter: parent.horizontalCenter
            y: height * - 0.05
            radius: width
        }

        Row {
            id: gaugeRow
            spacing: 30
            anchors.centerIn: parent

            TurnIndicator {
                id: leftIndicator
                anchors.verticalCenter: parent.verticalCenter
                width: height
                height: container.height * 0.5 - gaugeRow.spacing

                direction: Qt.LeftArrow
                on: false
            }
            CircularGauge {
                id: fuelGauge
                maximumValue: 1
                value: (slideCount - currentSlide - 1) / (slideCount)
                width: height
                height: container.height * 0.5
                y: height * 0.7

                style: IconGaugeStyle {
                    id: fuelGaugeStyle

                    icon: "./images/fuel-icon.png"
                    minWarningColor: Qt.rgba(0.5, 0, 0, 1)

                    tickmarkLabel: Text {
                        color: "white"
                        visible: styleData.value === 0 || styleData.value === 1
                        font.pixelSize: fuelGaugeStyle.toPixels(0.225)
                        text: styleData.value === 0 ? "E" : (styleData.value === 1 ? "F" : "")
                    }
                }

                Behavior on value {
                    NumberAnimation {
                        duration: animTime
                        easing.type: Easing.InOutSine
                    }
                }
            }

            CircularGauge {
                id: speedometer
                anchors.verticalCenter: parent.verticalCenter
                maximumValue: slideCount * 10
                value: (currentSlide + 1) * 10
                width: height
                height: container.height

                style: DashboardGaugeStyle {}

                Behavior on value {
                    NumberAnimation {
                        duration: animTime
                        easing.type: Easing.InOutSine
                    }
                }
            }

            CircularGauge {
                id: tachometer
                width: height
                height: container.height - gaugeRow.spacing
                maximumValue: 8
                value: 2
                anchors.verticalCenter: parent.verticalCenter

                style: TachometerStyle {
                    rpmTextValue: tachometer.value
                }
            }

            TurnIndicator {
                id: rightIndicator
                anchors.verticalCenter: parent.verticalCenter
                width: height
                height: container.height * 0.5 - gaugeRow.spacing

                direction: Qt.RightArrow
                on: false
            }
        }
    }

    SequentialAnimation {
        id: rpmAnim
        NumberAnimation { target: tachometer; property: "value"; duration: animTime / 2; to: 8; easing.type: Easing.InOutQuad }
        NumberAnimation { target: tachometer; property: "value"; duration: animTime / 2; to: 2; easing.type: Easing.InOutQuad }
    }

    SequentialAnimation {
        id: turnLeftAnim
        PropertyAnimation { target: leftIndicator; property: "on"; to: true }
        PropertyAnimation { target: leftIndicator; property: "on"; to: false; duration: animTime }
    }

    SequentialAnimation {
        id: turnRightAnim
        PropertyAnimation { target: rightIndicator; property: "on"; to: true }
        PropertyAnimation { target: rightIndicator; property: "on"; to: false; duration: animTime }
    }
}
