import QtQuick 2.5
import Qt.labs.presentation 1.0

Presentation
{
    id: root

    width: 1280
    height: 720

    textColor: "white"
    titleColor: "red"

    Rectangle {
        anchors.fill: parent
        color: Qt.lighter("#161616")
    }

    SlideTransition {
        id: slideTransition
        fromSlide: slides[currentSlide]
        toSlide: slides[currentSlide + 1]
    }

    Dashboard {
        id: dashboard
        slideCount: slides.length
        currentSlide: root.currentSlide
    }

    SlideCounter {}
    Clock {}

    function switchSlides(from, to, forward)
    {
        var ret = slideTransition.switchSlides(from, to, forward);
        if (ret) {
            if (forward)
                dashboard.turnRight();
            else
                dashboard.turnLeft();
        }
        return ret
    }

    Slide {
        textColor: "red"
        centeredText: "the Anti Theft Auto"
        fontSize: 50
    }

    Slide {
        title: "Motivation"
        CenterImage {
            source: "./images/motivation_1.jpg"
        }
    }

    Slide {
        title: "Motivation"
        content: [
            "Problems with \"cheap\" GPS trackers:",
            " 3rd party web",
            " not user friendly",
            " does not alarm",
            " battery life",
            " trust issues"
        ]
        RightImage {
            source: "./images/gt03.jpg"
            x: parent.width / 2 + width / 2
        }
    }

    Slide {
        title: "Motivation"
        LeftImage {
            source: "./images/solarpower.jpg"
        }
        CenterImage {
            source: "./images/challenge-accepted.png"
            height: parent.height * 0.5
        }
        RightImage {
            source: "./images/htc200.jpg"
            x: parent.width / 2 + width / 2
        }
    }

    Slide {
        title: "Motivation"
        CenterImage {
            source: "./images/message.jpg"
            height: parent.height * 0.8
        }
    }

    Slide {
        title: "Early problems"
        RightImage {
            source: "./images/fat-battery.jpg"
        }
        content: [
            "Idea - hide in a stuffed animal under the rear window",
            " stupid charging animation",
            " heat kills the phone",
            " battery almost exploded",
            "Current solution",
            " phone is charged when the engine is on"
        ]
    }

    Slide {
        title: "Project goals"
        content: [
            "Features",
            " internet connection must be optional",
            " must work without interactions",
            " SMS API is a must",
            " be configurable for different use cases",
            "Applications",
            " separate apps <-> permissions"
        ]
        RightImage {
            source: "./images/challenge-accepted.png"
            height: parent.height * 0.5
            x: parent.width / 2 + width / 2
        }
    }

    Slide {
        title: "Applications"
        CenterImage {
            source: "./images/project-goal.png"
        }
    }

    Slide {
        title: "Protector"
        LeftImage {
            source: "./images/p1.png"
            height: parent.height * 0.8
        }
        CenterImage {
            source: "./images/p2.png"
            height: parent.height * 0.8
        }
        RightImage {
            source: "./images/p3.png"
            height: parent.height * 0.8
        }
    }

    Slide {
        title: "Watcher GCM / SMS | SMS"
        LeftImage {
            source: "./images/w1.png"
            height: parent.height * 0.8
        }
        CenterImage {
            source: "./images/w2.jpg"
            height: parent.height * 0.8
        }
        RightImage {
            source: "./images/w3.png"
            height: parent.height * 0.8
        }
    }

    Slide {
        title: "Gradle"
        content: [
            " Groovy + DSL",
            " Built-in dependency management",
            " IDE integration",
            " org.gradle.daemon=true",
            " https://developer.android.com\n/tools/building/configuring-gradle.html",
            " http://gradleplease.appspot.com/",
        ]
        CodeSection {
            text: "apply plugin: 'com.android.application'

android {
  compileSdkVersion 22
  buildToolsVersion \"21.1.2\"

  defaultConfig { }
  buildTypes { }
}

dependencies {
  compile fileTree(dir: 'libs', include: ['*.jar'])
  compile 'com.android.support:appcompat-v7:22.1.1'
  compile project(':WatcherCommon')
  testCompile 'junit:junit:4.12'
}
"
        }
    }

    Slide {
        title: "Communication - POJO"
        content: [
            " JSON / YAML / BASE64 PROTOBUF",
            "  Goal: message length <= 140",
            "  textual",
            " Custom serialization with reflection",
            "   * 3f 21i3v9 21ayeo fpmbaa67 string ** \n_a_b_c_d_e_f_g_h_ -> 55",
            "   {\"b\":{\"a\":123,\"b\":123456789,\"c\":{\"a\":123.12312,\n\"b\":123.1231231231}},\"c\":\"string\",\"d\":\"*\",\n\"e\":\" a b c d e f g h \"} -> 113",
            "   EhcIexCVmu86Gg4NCj/2QhEjxs4/4cdeQBoGc3RyaW5nIg\nEqKhEgYSBiIGMgZCBlIGYgZyBoIA== -> 76"
        ]
        CodeSection {
            text: "public class Protector {
  @Order(0) public CarLocation carLocation;
  @Order(1) public ParkLocation parkLocation;
  @Order(2) public Status status;
  @Order(3) public Service service;
}

public class Serializer {
  public static String write(Object object) {
      return Parizer.serialize(object);
  }
  public static <T> T
  read(String string, Class<T> clazz) {
      return Parizer.deserialize(string, clazz);
  }
}"
        }
    }

    Slide {
        title: "Protector's architecture"
        content: [
            " http://developer.android.com\n/guide/components/services.html",
            " ProtectorService",
            "  foreground",
            "  command queue & scheduling",
            "  different receivers",
            " WakefulBroadcastReceivers",
            "  SMS",
            "  GCM",
            "  AlarmManager"
        ]
        CodeSection {
            text: "<service android:name=\"com.example.MyService\"/>

<receiver android:name=\"com.example.BootReceiver\"
  android:enabled=\"false\">
  <intent-filter>
    <action android:name=\"android.intent.action
                           .BOOT_COMPLETED\"/>
  </intent-filter>
</receiver>

ComponentName r =
   new ComponentName(c, BootReceiver.class);
PackageManager p = c.getPackageManager();
p.setComponentEnabledSetting(..);
"
        }
    }

    Slide {
        title: "Google APIs"
        content: [
            " https://console.developers.google.com",
            "  create project / enable APIs",
            "  manage credentials (keys)",
            " GoogleCloudMessaging",
            "  android_key for receiving messages",
            "  project_id for register gcm_id",
            " com.ganyo:gcm-server:1.0.2",
            "  sender = new Sender(server_key)",
            "  sender.sendMessage(msg, gcm_id)",
        ]
        RightImage {
            source: "./images/gcm.png"
            y: (parent.height / 2 - parent.height / 3) - height / 2
        }
        RightImage {
            source: "./images/maps.png"
            y: (parent.height / 2 + parent.height / 10) - height / 2
        }
    }

    Slide {
        title: "GCM delay"
        centeredText: "Push Notifications Fixer\n\ncom.google.android.intent.action.MCS_HEARTBEAT\ncom.google.android.intent.action.GTALK_HEARTBEAT"
    }

    Slide {
        title: "Location strategies"
        content: [
            " http://developer.android.com/guide/\ntopics/location/strategies.html",
            " http://developer.android.com/training/\nlocation/index.html",
            " ERROR HANDLING!!!",
            " Geofencing was not\nconvenient enough"
        ]
        CodeSection {
            text: "new GoogleApiClient.Builder(context)
.addApi(LocationServices.API)
.addConnectionCallbacks(this)
.addOnConnectionFailedListener(this)
.build();

request = LocationRequest.create();
request.setPriority(LocationRequest.
                    PRIORITY_HIGH_ACCURACY);
request.setInterval(5000);
request.setFastestInterval(1000);

LocationServices.FusedLocationApi
.requestLocationUpdates(gApiClient, request, this);"
        }
    }

    Slide {
        title: "Hack"
        centeredText: "SCREEN_DIM_WAKE_LOCK"
    }

    Slide {
        AnimatedImage {
            width: parent.width * 0.5
            x: parent.width / 2 - width / 2
            y: (parent.height / 2 - parent.height / 8) - height / 2
            source: "./images/cat-1.gif"
        }
    }

    Slide {
        AnimatedImage {
            width: parent.width * 0.5
            x: parent.width / 2 - width / 2
            y: (parent.height / 2 - parent.height / 8) - height / 2
            source: "./images/publish.gif"
        }
    }

    Slide {
        textColor: "red"
        centeredText: "Thank you! Questions?"
    }
}
